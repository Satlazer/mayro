﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialScaler : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		Vector2 vec = -this.transform.localScale; 
		GetComponent<MeshRenderer>().material.SetVector("_MainTex_ST", vec);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
