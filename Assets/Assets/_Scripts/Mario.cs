﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using UnityEngine.UI;
 


[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody))]
public class Mario : MonoBehaviour 
{
	public Camera cam;
	public AudioClip aud;
	public GameObject HUD;
	public GameObject FireBall;
	Text text;
	Rigidbody rb;
	public float moveSpeed = 20.0f; 
	public float jumpHeight = 3000.0f;
	public Vector3 maxSpeed = new Vector3(50.0f, 10000.0f, 0.0f);
	public Vector3 maxRunSpeed = new Vector3(100.0f, 10000.0f, 0.0f);
	
	public AnimationCurve JumpSpeed;
	public float JumpTime = 0.4f;
	public float jumpTimer = 0.0f;
	public bool Jumped = false;
	bool shootingFire = false;

	SerialPort stream;
	// Use this for initialization
	void Start () 
	{
		cam = Camera.main;
		rb = GetComponent<Rigidbody>();
		text = HUD.GetComponent<Text>();

		stream = new SerialPort("COM5", 9600);
		stream.ReadTimeout = 50;
		try
		{
		stream.Open();
		}
		catch(System.Exception)
		{

		}
	}
	
	void FixedUpdate()
	{
		
		if(Jumped)
		{
			jumpTimer += Time.fixedDeltaTime;
		}
		if(jumpTimer < JumpTime && jumpTimer > 0.0f)
		{
			rb.AddForce(Vector3.up * JumpSpeed.Evaluate(jumpTimer / JumpTime) * jumpHeight);
		}
	}

	void OnCollisionEnter(Collision other)
	{
		if(rb.velocity.y <= 0.1f)
		{
			Jumped = false;
		}
	}

	//public IEnumerator AsynchronousReadFromArduino(Action<string> callback, Action fail = null, float timeout = float.PositiveInfinity) 
	//{
	//	DateTime initialTime = DateTime.Now;
	//	DateTime nowTime;
	//	TimeSpan diff = default(TimeSpan);
	//
	//	string dataString = null;
	//
	//	do {
	//	    try {
	//	        dataString = stream.ReadLine();
	//	    }
	//	    catch (TimeoutException) {
	//	        dataString = null;
	//	    }
	//
	//	    if (dataString != null)
	//	    {
	//	        callback(dataString);
	//	        yield return null;
	//	    } else
	//	        yield return new WaitForSeconds(0.05f);
	//
	//	    nowTime = DateTime.Now;
	//	    diff = nowTime - initialTime;
	//
	//	} while (diff.Milliseconds < timeout);
	//
	//	if (fail != null)
	//	    fail();
	//	yield return null;
	//}

	// Update is called once per frame
	void Update () 
	{
		Vector3 pos = cam.transform.position;
		pos.x = Mathf.Max(12.45f, this.transform.position.x);
		cam.transform.position = pos;


		float MoveDirection = 0.0f;
		int buttonA = 0;
		int buttonB = 0;

		try
		{
			string test = stream.ReadLine();
			string[] values = test.Split(',');
			MoveDirection = System.Convert.ToInt32(values[0]);
			MoveDirection /= 512.0f;
			MoveDirection -= 1.0f;
			MoveDirection = -MoveDirection;
			buttonA = System.Convert.ToInt32(values[1]);
			buttonB = System.Convert.ToInt32(values[2]);
			text.text = "Pote: " + values[0] + "\n";
			text.text += "Button 1: " + values[1] + "\n";
			text.text += "Button 2: " + values[2] + "\n";
        }
        catch(System.Exception)
		{
			print("OOF, NO ARDUINO");
			text.text = "OOF, NO ARDUINO";
        }

		if(MoveDirection > 0.0f)
		{
			if(rb.velocity.x < maxSpeed.x)
				rb.AddForce(Vector3.right * MoveDirection * moveSpeed);
		}
		if(MoveDirection < 0.0f)
		{
			if(rb.velocity.x > -maxSpeed.x)
				rb.AddForce(Vector3.right * MoveDirection * moveSpeed);
		}

		if(buttonB == 0 && !Input.GetKey(KeyCode.LeftShift))
		{
			shootingFire = false;
		}

		if(shootingFire == false && (buttonB == 1 || Input.GetKey(KeyCode.LeftShift)))
		{
			shootingFire = true;
			Instantiate(FireBall, this.transform.position + Vector3.right, this.transform.rotation);
		}

		if(Input.GetKey(KeyCode.Space) || buttonA == 1)
		{
			if(Jumped == false)
			{
				jumpTimer = 0.0f;
			}

			Jumped = true;
			//rb.AddForce(Vector3.up * jumpHeight);
		}
		if(Input.GetKeyUp(KeyCode.Space))
		{
			jumpTimer = JumpTime;
		}
		if(Input.GetKey(KeyCode.D))
		{
			if(rb.velocity.x < maxSpeed.x)
				rb.AddForce(Vector3.right * moveSpeed);
		}
		if(Input.GetKey(KeyCode.A))
		{
			if(rb.velocity.x > -maxSpeed.x)
				rb.AddForce(Vector3.left * moveSpeed);
		}

		//if(buttonB == 1)
		//{
		//	GetComponent<AudioSource>().PlayOneShot(aud);
		//	Instantiate(this.gameObject, this.transform.position, this.transform.rotation);
		//}
				
		pos = this.transform.position;
		pos.x = Mathf.Max(0.5f, this.transform.position.x);
		this.transform.position = pos;
	}

}
