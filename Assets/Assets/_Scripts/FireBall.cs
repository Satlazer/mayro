﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FireBall : MonoBehaviour 
{
	public float velocity = 100.0f;
	Rigidbody rb;

	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody>(); 
		rb.AddForce(new Vector3(velocity, -velocity / 5.0f, 0.0f));
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
