﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompositeShader : MonoBehaviour 
{
	public Shader shader;
	Material material;


	// Use this for initialization
	void Start () 
	{
		material = new Material(shader);
	}
	void OnRenderImage(RenderTexture src, RenderTexture dest)
	{
		Graphics.Blit(src, dest, material);
	}
}
